#!/bin/bash

# This script syncs this local directory with the artifactory.
#
# To see what files Doop expects for each Android version, see its
# AnalysisFactory.platform() method.
#
# Currently the following versions are fully supported (for space
# considerations): 15, 16, 22, 24, 25.

set -e

# Download android.jar.
wget_android_jar () {
    cd android-$1
    rm -f android.jar
    wget http://centauri.di.uoa.gr:8081/artifactory/list/Platforms/Android/stubs/Android/Sdk/platforms/android-$1/android.jar
    cd ..
}

# Download uiautomator.jar.
wget_uiautomator_jar () {
    cd android-$1
    rm -f uiautomator.jar
    wget http://centauri.di.uoa.gr:8081/artifactory/list/Platforms/Android/stubs/Android/Sdk/platforms/android-$1/uiautomator.jar
    cd ..
}

# Download data/layoutlib.jar.
wget_data_layoutlib_jar () {
    cd android-$1/data
    rm -f layoutlib.jar
    wget http://centauri.di.uoa.gr:8081/artifactory/list/Platforms/Android/stubs/Android/Sdk/platforms/android-$1/data/layoutlib.jar
    cd ../..
}

# Download data/icu4j.jar.
wget_data_icu4j_jar () {
    cd android-$1/data
    rm -f icu4j.jar
    wget http://centauri.di.uoa.gr:8081/artifactory/list/Platforms/Android/stubs/Android/Sdk/platforms/android-$1/data/icu4j.jar
    cd ../..
}

# Download android-stubs-src.jar.
wget_android_stubs_src_jar () {
    cd android-$1
    rm -f android-stubs-src.jar
    wget http://centauri.di.uoa.gr:8081/artifactory/list/Platforms/Android/stubs/Android/Sdk/platforms/android-$1/android-stubs-src.jar
    cd ..
}

# Download optional/org.apache.http.legacy.jar.
wget_optional_apache_jar () {
    cd android-$1/optional
    rm -f org.apache.http.legacy.jar
    wget http://centauri.di.uoa.gr:8081/artifactory/list/Platforms/Android/stubs/Android/Sdk/platforms/android-$1/optional/org.apache.http.legacy.jar
    cd ../..
}

# android-7
wget_android_jar 7
wget_data_layoutlib_jar 7
# android-15
wget_android_jar 15
wget_data_layoutlib_jar 15
# android-16
wget_android_jar 16
wget_data_layoutlib_jar 16
wget_uiautomator_jar 16
# android-17
wget_android_jar 17
wget_data_icu4j_jar 17
wget_data_layoutlib_jar 17
wget_uiautomator_jar 17
# android-18
wget_android_jar 18
wget_data_icu4j_jar 18
wget_data_layoutlib_jar 18
wget_uiautomator_jar 18
# android-19
wget_android_jar 19
wget_data_icu4j_jar 19
wget_data_layoutlib_jar 19
wget_uiautomator_jar 19
# android-20
wget_android_jar 20
wget_data_icu4j_jar 20
wget_data_layoutlib_jar 20
wget_uiautomator_jar 20
# android-21
wget_android_jar 21
wget_data_icu4j_jar 21
wget_data_layoutlib_jar 21
wget_uiautomator_jar 21
# android-22
wget_android_jar 22
wget_data_icu4j_jar 22
wget_data_layoutlib_jar 22
wget_uiautomator_jar 22
# android-23
wget_android_jar 23
wget_data_layoutlib_jar 23
wget_optional_apache_jar 23
wget_uiautomator_jar 23
# android-24
wget_android_stubs_src_jar 24
wget_android_jar 24
wget_data_layoutlib_jar 24
wget_optional_apache_jar 24
wget_uiautomator_jar 24
# android-25
wget_android_stubs_src_jar 25
wget_android_jar 25
wget_data_layoutlib_jar 25
wget_optional_apache_jar 25
wget_uiautomator_jar 25
