#!/bin/bash
CWD=`pwd`
TMP1=`mktemp -d`
TMP2=`mktemp -d`

echo TMP1 = ${TMP1}
echo TMP2 = ${TMP2}

for d in ${CWD}/play-services-*
do
    AAR=`find ${d} -name "*.aar" | sort | head -1`
    echo AAR = ${AAR}
    rm -rf ${TMP1}/*
    cd ${TMP1}
    unzip ${AAR} > /dev/null
    cd ${TMP2}
    mv ${TMP1}/classes.jar $(basename ${d}).jar
done

mv ${TMP2} ${CWD}
rm -rf ${TMP1}
